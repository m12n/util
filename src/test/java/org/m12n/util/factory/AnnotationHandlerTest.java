package org.m12n.util.factory;

import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.m12n.util.factory.ProcessingScope.CONSTRUCT_TYPE;
import static org.m12n.util.factory.ProcessingScope.DEFAULT;
import static org.m12n.util.factory.ProcessingScope.POSTPROCESS_TYPE;
import static org.m12n.util.factory.ProcessingScope.PREPROCESS_TYPE;
import static org.m12n.util.factory.ProcessingScope.PROCESS_FIELD;

public class AnnotationHandlerTest {
    @Before
    public void reset() {
        AnnotationHandler.reset();
    }

    static class AP1 extends TypeConstructor<Object, A1> {
        @Override
        protected Object constructType(final Class<Object> type, final A1 annotation) throws Exception {
            return Factory.create(annotation.value());
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @ProcessedBy(value = AP1.class, scope = CONSTRUCT_TYPE)
    @interface A1 {
        Class<?> value();
    }

    static class C1 implements I1, I1A {
        @Override
        public String value() {
            return "Foo";
        }
    }

    @A1(C1.class)
    static interface I1 {
        String value();
    }

    @Test
    public void verifyConstructType() {
        assertThat(AnnotationHandler.process(null, I1.class).value(), is("Foo"));
    }

    @ProcessedBy(value = AP1.class, scope = CONSTRUCT_TYPE)
    static interface I1A {
        String value();
    }

    @Test
    public void verifyConstructTypeWithDirectAnnotation() {
        assertThat(AnnotationHandler.process(null, I1.class).value(), is("Foo"));
    }

    static class AP2 extends TypeProcessor<Object, Annotation> {
        @Override
        protected void processType(final Object obj, final Annotation annotation) throws Exception {
            final String value = annotation instanceof A2 ? ((A2) annotation).value() : "Bar";
            obj.getClass().getDeclaredField("value").set(obj, value);
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @ProcessedBy(value = AP2.class, scope = PREPROCESS_TYPE)
    @interface A2 {
        String value();
    }

    @A2("Foo")
    static class C2 {
        String value;
    }

    @Test
    public void verifyPreprocessType() throws Exception {
        final Field field = C2.class.getDeclaredField("value");
        final C2 c2 = new C2();
        field.set(c2, "Foo");
        assertThat(c2.value, is("Foo"));
        assertThat(AnnotationHandler.process(new C2()).value, is("Foo"));
    }

    @ProcessedBy(value = AP2.class, scope = PREPROCESS_TYPE)
    static class C2A {
        String value;
    }

    @Test
    public void verifyPreprocessTypeWithDirectAnnotation() {
        assertThat(AnnotationHandler.process(new C2A()).value, is("Bar"));
    }

    static class AP3 extends FieldProcessor<Object, Annotation> {
        @Override
        protected void processField(final Object obj, final Field field, final Annotation annotation) throws Exception {
            final String value = annotation instanceof A3 ? ((A3) annotation).value() : "Bar";
            field.set(obj, value);
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @ProcessedBy(value = AP3.class)
    @interface A3 {
        String value() default "";
    }

    static class C3 {
        @A3("Foo")
        String value;
    }

    @Test
    public void verifyProcessField() {
        assertThat(AnnotationHandler.process(new C3()).value, is("Foo"));
    }

    static class C3A {
        @ProcessedBy(AP3.class)
        String value;
    }

    @Test
    public void verifyProcessFieldWithDirectAnnotation() {
        assertThat(AnnotationHandler.process(new C3A()).value, is("Bar"));
    }

    static class AP4 extends TypeProcessor<C4, A4> {
        @Override
        protected void processType(final C4 obj, final A4 annotation) throws Exception {
            obj.value = annotation.value();
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @ProcessedBy(value = AP4.class, scope = POSTPROCESS_TYPE)
    @interface A4 {
        String value();
    }

    @A4("Foo")
    static class C4 {
        String value;
    }

    @Test
    public void verifyPostprocessType() {
        assertThat(AnnotationHandler.process(new C4()).value, is("Foo"));
    }

    static class C5 {
        @Inject
        String value;
    }

    @Test
    public void verifyRegisterFieldProcessor() throws Exception {
        AnnotationHandler.register(
                Inject.class,
                new FieldProcessor<C5, Annotation>() {
                    @Override
                    protected void processField(final C5 obj, final Field field, final Annotation annotation) throws Exception {
                        field.set(obj, "Foo");
                    }
                },
                PROCESS_FIELD);
        final C5 c5 = new C5();
        assertThat(AnnotationHandler.process(c5).value, is("Foo"));
    }

    @Resource(description = "Foo")
    static class C6 {
        String value;
    }

    @Test
    public void verifyRegisterTypePreprocessor() {
        AnnotationHandler.register(
                Resource.class,
                (AnnotationProcessor<C6, Resource>) (type, obj, field, annotation) -> {
                    obj.value = annotation.description();
                    return obj;
                },
                PREPROCESS_TYPE);
        assertThat(AnnotationHandler.process(new C6()).value, is("Foo"));
    }

    @Resource
    interface I7 {
    }

    @Test
    public void verifyRegisterTypeConstructor() {
        AnnotationHandler.register(
                Resource.class,
                (AnnotationProcessor<I7, Resource>) (type, obj, field, annotation) -> new I7() {
                },
                CONSTRUCT_TYPE);
        assertThat(AnnotationHandler.process(null, I7.class), is(not(nullValue())));
    }

    @Resource
    static class C8 {
        String value;
    }

    @Test
    public void verifyRegisterTypePostprocessor() {
        AnnotationHandler.register(
                Resource.class,
                new TypeProcessor<C8, Resource>() {
                    @Override
                    protected void processType(final C8 obj, final Resource annotation) throws Exception {
                        obj.value = "Foo";
                    }
                },
                POSTPROCESS_TYPE);
        assertThat(AnnotationHandler.process(new C8()).value, is("Foo"));
    }

    @Test(expected = IllegalStateException.class)
    public void verifyProcessWithException() {
        AnnotationHandler.register(
                Resource.class,
                new TypeProcessor<C8, Resource>() {
                    @Override
                    protected void processType(final C8 obj, final Resource annotation) throws Exception {
                        throw new Exception("Error");
                    }
                },
                PREPROCESS_TYPE
        );
        AnnotationHandler.process(new C8());
    }

    @Test(expected = IllegalArgumentException.class)
    public void verifyRegisterDefaultScope() {
        AnnotationHandler.register(Resource.class, null, DEFAULT);
    }

    @Test(expected = IllegalProcessingScopeException.class)
    public void verifyRegisterIllegalScope() {
        AnnotationHandler.register(
                Resource.class,
                new FieldProcessor() {
                    @Override
                    protected void processField(final Object obj, final Field field, final Annotation annotation) throws Exception {
                    }
                },
                PROCESS_FIELD);
        AnnotationHandler.process(new C8());
    }

    static class AP9 extends FieldProcessor {
        @Override
        protected void processField(final Object obj, final Field field, final Annotation annotation) throws Exception {
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @ProcessedBy(value = AP9.class, scope = PROCESS_FIELD)
    @interface A9 {
    }

    @A9
    static class C9 {
    }

    @Test(expected = IllegalProcessingScopeException.class)
    public void verifyIllegalScope() {
        AnnotationHandler.process(new C9());
    }

    @Test(expected = FactoryException.class)
    public void verifyPrivateConstructor() {
        Factory.newInstance(AnnotationHandler.class);
    }
}
