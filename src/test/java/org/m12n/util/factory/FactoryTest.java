package org.m12n.util.factory;

import org.junit.Before;
import org.junit.Test;
import org.m12n.util.factory.other.HelperInterfaceForTestingFactoryWithAnnotation;

import javax.inject.Inject;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.m12n.util.factory.Factory.getInstance;

public class FactoryTest {
    static class MyClass {
        static int instanceCounter;

        public MyClass() {
            instanceCounter++;
        }
    }

    @Before
    public void reset() {
        Factory.reset();
    }

    @Test
    public void ensureInstanceIsCreatedOnlyOnce() {
        assertEquals(0, MyClass.instanceCounter);
        final MyClass myClass1 = getInstance(MyClass.class);
        assertEquals(1, MyClass.instanceCounter);
        final MyClass myClass2 = getInstance(MyClass.class);
        assertEquals(1, MyClass.instanceCounter);
        assertSame(myClass1, myClass2);
    }

    static class MyOtherClass {
    }

    @Test
    public void ensureInstantiateNonPublicClass() {
        assertNotNull(getInstance(MyOtherClass.class));
    }

    @Test(expected = RuntimeException.class)
    public void ensureInstantiateInterfaceThrowsException() {
        getInstance(HelperInterfaceForTestingFactoryWithoutAnnotation.class);
    }

    @Test
    public void ensureInstantiateInterfaceWithAnnotation() {
        assertNotNull(getInstance(HelperInterfaceForTestingFactoryWithAnnotation.class));
    }

    @Test(expected = IllegalAccessError.class)
    public void ensurePrivateConstructor() {
        Factory.create(Factory.class);
    }

    static class Class6 {
        public String val() {
            return "Foo";
        }
    }

    @Test
    public void ensureDefine() {
        Factory.define(Class6.class, new Class6() {
            @Override
            public String val() {
                return "Bar";
            }
        });

        final Class6 class6 = Factory.inject(Class6.class);
        assertEquals("Bar", class6.val());
    }

    static class Class7 {
        String greet(final String name) {
            return "Hello, " + name + "!";
        }
    }

    static class Class8 {
        @Inject
        private Class7 class7;

        String sayHello() {
            return class7.greet("World");
        }
    }

    @Test
    public void verifyInjectAnnotation() {
        assertThat(Factory.create(Class8.class).sayHello(), is("Hello, World!"));
    }
}
