package org.m12n.util;

import org.junit.Test;
import org.m12n.util.factory.Factory;
import org.m12n.util.function.ThrowingRunnable;

import java.io.IOException;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class ExceptionsTest {
    @Test
    public void verifySneakyThrowsWithRunnable() {
        executeTestWithRunnable(Exceptions::sneakyThrows);
        try {
            Exceptions.sneakyThrows((ThrowingRunnable) () -> {
                throw new IOException();
            });
            fail("Exception expected");
        }
        catch (final IllegalStateException e) {
            assertEquals(IOException.class, e.getCause().getClass());
        }
    }

    private static void executeTestWithRunnable(final Consumer<ThrowingRunnable> consumer) {
        final int[] c = new int[1];
        c[0] = 1;

        consumer.accept(() -> {
            c[0]++;
        });
        assertEquals(2, c[0]);
    }

    @Test
    public void verifySneakyThrowsWithProducer() {
        final String s1 = Exceptions.sneakyThrows(() -> "Test");
        assertEquals("Test", s1);

        try {
            final String val = Exceptions.sneakyThrows(() -> {
                throw new IOException();
            });
            fail("Exception expected");
        }
        catch (final IllegalStateException e) {
            assertEquals(IOException.class, e.getCause().getClass());
        }
    }

    @Test
    public void verifySafeRunWithRunnable() throws Exception {
        executeTestWithRunnable(Exceptions::safeRun);
        Exceptions.safeRun((ThrowingRunnable) () -> {
            throw new IOException();
        });
    }

    @Test
    public void verifySafeRunWithRunnableAndConsumer() throws Exception {
        final String[] s = new String[1];
        Exceptions.safeRun(() -> {
            throw new IOException("Test");
        }, e -> {
            s[0] = e.getMessage();
        });
        assertEquals("Test", s[0]);
    }

    @Test
    public void verifySafeRunWithProducer() throws Exception {
        final String s1 = Exceptions.safeRun(() -> "Test").get();
        assertEquals("Test", s1);

        assertFalse(Exceptions.safeRun(() -> {
            throw new IOException();
        }).isPresent());
    }

    @Test
    public void verifySafeRunWithProducerAndConsumer() throws Exception {
        final String[] s = new String[1];
        Exceptions.safeRun(() -> {
            throw new IOException("Test");
        }, e -> {
            s[0] = e.getMessage();
        });
        assertEquals("Test", s[0]);
    }

    @Test(expected = RuntimeException.class)
    public void verifyPrivateConstructor() {
        Factory.newInstance(Exceptions.class);
    }
}
