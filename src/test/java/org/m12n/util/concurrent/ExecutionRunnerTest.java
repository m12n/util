package org.m12n.util.concurrent;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

public class ExecutionRunnerTest {
    @Test
    @Ignore
    public void verifyRunner() throws Exception {
        final Map<String, Long> status = new ConcurrentHashMap<>();
        final ExecutionRunner runner = new ExecutionRunner(Executors.newCachedThreadPool());
        runner.run(() -> {
            status.put("start", System.currentTimeMillis());
            Thread.sleep(10);
            status.put("stop", System.currentTimeMillis());
        });
        status.put("waiting", System.currentTimeMillis());
        runner.sync();
        status.put("done", System.currentTimeMillis());

        assertThat(status.toString(), status.size(), is(4));

        assertThat(delta(status, "start", "waiting"), is(lessThan(10L)));
        assertThat(delta(status, "start", "done"), is(greaterThanOrEqualTo(10L)));
        assertThat(delta(status, "stop", "done"), is(lessThan(delta(status, "start", "done"))));
    }

    private long delta(final Map<String, Long> status, final String key1, final String key2) {
        System.out.printf("%s: %d; %s: %d%n", key1, status.get(key1), key2, status.get(key2));
        return Math.abs(status.get(key1) - status.get(key2));
    }
}
