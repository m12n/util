package org.m12n.util;

import org.junit.Test;
import org.m12n.util.factory.Factory;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

public class MapsTest {
    @Test
    public void verifyAsMap() {
        final Map<String, Integer> ages = Maps.asMap("A", 12, "B", 13);
        assertEquals(2, ages.size());
        assertEquals(12, ages.get("A").intValue());
        assertEquals(13, ages.get("B").intValue());
    }

    @Test
    public void verifyAsMapWithCreator() {
        assertEquals(2, Maps.asMap(() -> new HashMap<>(2), "A", 12, "B", 13).size());
        assertEquals(2, Maps.asMap(TreeMap::new, "A", 12, "B", 13).size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void verifyAsMapWithIllegalArgs() {
        Maps.asMap("A", 12, "B", 13, "C");
    }

    @Test(expected = RuntimeException.class)
    public void verifyPrivateConstructor() {
        Factory.newInstance(Maps.class);
    }
}
