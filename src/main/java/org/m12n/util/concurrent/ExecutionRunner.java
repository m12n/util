package org.m12n.util.concurrent;

import org.m12n.util.Exceptions;
import org.m12n.util.function.ThrowingRunnable;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class ExecutionRunner {
    private final ExecutorService executorService;
    private final Queue<Future<?>> futures = new ConcurrentLinkedQueue<>();

    public ExecutionRunner(final ExecutorService executorService) {
        this.executorService = executorService;
    }

    public void run(final ThrowingRunnable<?> runnable) {
        final Future<?> future = executorService.submit(() -> Exceptions.sneakyThrows(runnable));
        futures.offer(future);
    }

    public void sync() {
        while (!futures.isEmpty()) {
            Exceptions.sneakyThrows(() -> futures.remove().get());
        }
        executorService.shutdown();
    }
}
