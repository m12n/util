package org.m12n.util;

import java.util.Collection;
import java.util.Map;

/**
 * A simple multimap implementation, which maps a key to a {@link Collection} of values.
 *
 * @see Maps#newMultiMap()
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public interface MultiMap<K, V> extends Map<K, Collection<V>> {
    /**
     * Adds a single value to the collection of values for the given key.
     *
     * @param key   The key
     * @param value The value
     */
    void addTo(final K key, final V value);
}
