package org.m12n.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Utility methods for {@link Map}s
 */
public class Maps {
    private Maps() {
        throw new IllegalAccessError("Don't instantiate");
    }

    /**
     * Creates a new {@link Map} with the given content.
     *
     * @param key1   The first key
     * @param value1 The first value
     * @param args   The remaining keys and values, can be empty, but must be an even number of elements.
     * @param <K>    The key type
     * @param <V>    The value type
     * @return A new {@link Map}
     * @throws IllegalArgumentException if the {@code args} are not even.
     */
    public static <K, V> Map<K, V> asMap(final K key1, final V value1, final Object... args) {
        return asMap(HashMap::new, key1, value1, args);
    }

    /**
     * Creates a new {@link Map} with the given content.
     *
     * @param creator A {@link Supplier} that creates an empty map. Useful if not the default implementation should be
     *                used.
     * @param key1    The first key
     * @param value1  The first value
     * @param args    The remaining keys and values, can be empty, but must be an even number of elements.
     * @param <K>     The key type
     * @param <V>     The value type
     * @return A new {@link Map}
     * @throws IllegalArgumentException if the {@code args} are not even.
     */
    public static <K, V> Map<K, V> asMap(final Supplier<Map<K, V>> creator, final K key1, final V value1, final Object... args) {
        final int numArgs = args.length;
        if (numArgs % 2 != 0) {
            throw new IllegalArgumentException("Illegal number of arguments: " + (2 + numArgs));
        }
        final Map<K, V> map = creator.get();
        map.put(key1, value1);
        for (int i = 0; i < numArgs; ) {
            final K key = (K) args[i++];
            final V value = (V) args[i++];
            map.put(key, value);
        }
        return map;
    }

    /**
     * Creates a new simple {@link MultiMap} based on a {@link HashMap} and a {@link HashSet}.
     *
     * @param <K> The key type
     * @param <V> The value type
     * @return A {@link MultiMap} instance
     */
    public static <K, V> MultiMap<K, V> newMultiMap() {
        class MultiHashMap extends HashMap<K, Collection<V>> implements MultiMap<K, V> {
            @Override
            public void addTo(final K key, final V value) {
                computeIfAbsent(key, k -> new HashSet<>()).add(value);
            }
        }
        return new MultiHashMap();
    }
}
