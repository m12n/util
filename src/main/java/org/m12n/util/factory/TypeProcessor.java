package org.m12n.util.factory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Abstract class to process a type at {@link ProcessingScope#PREPROCESS_TYPE}
 * or {@link ProcessingScope#POSTPROCESS_TYPE}.
 *
 * @param <T> Type of the object
 * @param <A> Type of the annotation
 */
public abstract class TypeProcessor<T, A extends Annotation> implements AnnotationProcessor<T, A> {
    protected abstract void processType(T obj, A annotation) throws Exception;

    @Override
    public final T apply(final Class<T> type, final T obj, final Field field, final A annotation) throws Exception {
        processType(obj, annotation);
        return obj;
    }
}
