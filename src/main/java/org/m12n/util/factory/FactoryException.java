package org.m12n.util.factory;

/**
 * Thrown by the {@link Factory} when a class couldn't be created.
 */
public class FactoryException extends RuntimeException {
    public <T> FactoryException(final Class<T> type, final Exception e) {
        super("Unable to create type " + type, e);
    }
}
