package org.m12n.util.factory;

class DefaultImplTypeConstructor extends TypeConstructor<Object, DefaultImpl> {
    @Override
    protected Object constructType(final Class<Object> type, final DefaultImpl annotation) throws Exception {
        return Factory.create(annotation.value());
    }
}
