package org.m12n.util.factory;

/**
 * Defines at which lifecycle stage the annotation is processed.
 */
public enum ProcessingScope {
    /**
     * Before constructing the type (can be used to create instances of interfaces or abstract classes).
     */
    CONSTRUCT_TYPE(true, false),
    /**
     * Before processing fields
     */
    PREPROCESS_TYPE(true, false),
    /**
     * After processing fields
     */
    POSTPROCESS_TYPE(true, false),
    /**
     * Processes fields
     */
    PROCESS_FIELD(false, true),
    /**
     * Default, depending on where the annotation is attached (type or field), see {@link ProcessingScopeLevel}
     */
    DEFAULT(true, true);

    private final boolean isType;
    private final boolean isField;

    private static final ProcessingScope[] PROCESSING_ORDER = {
            CONSTRUCT_TYPE,
            PREPROCESS_TYPE,
            PROCESS_FIELD,
            POSTPROCESS_TYPE};

    ProcessingScope(final boolean isType, final boolean isField) {
        this.isType = isType;
        this.isField = isField;
    }

    /**
     * @return true, if the scope is applied at type level
     */
    public boolean isType() {
        return isType;
    }

    /**
     * @return true, if the scope is applied at field level
     */
    public boolean isField() {
        return isField;
    }

    /**
     * @return the order of scopes being processed
     */
    public static ProcessingScope[] processingOrder() {
        return PROCESSING_ORDER;
    }
}
