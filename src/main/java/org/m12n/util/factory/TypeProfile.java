package org.m12n.util.factory;

import org.m12n.util.Maps;
import org.m12n.util.MultiMap;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Consumer;

import static java.util.Arrays.stream;

class TypeProfile<T> {
    private final MultiMap<ProcessingScope, TypeConfig> typeConfigs = Maps.newMultiMap();

    TypeProfile(final Class type, final MultiMap<Class<? extends Annotation>, AnnotationConfig> annotationConfigs) {
        addTypeConfigs(annotationConfigs, type, null, ProcessingScopeLevel.TYPE);
        for (final Field field : type.getDeclaredFields()) {
            field.setAccessible(true);
            addTypeConfigs(annotationConfigs, field, field, ProcessingScopeLevel.FIELD);
        }
    }

    private void addTypeConfigs(
            final MultiMap<Class<? extends Annotation>, AnnotationConfig> annotationConfigs,
            final AnnotatedElement element,
            final Field field,
            final ProcessingScopeLevel level) {
        forEachAnnotation(element, elementAnnotation -> {
            final Class<? extends Annotation> annotationType = elementAnnotation.annotationType();

            final Collection<AnnotationConfig> configs = annotationConfigs.get(annotationType);
            if (configs != null && !configs.isEmpty()) {
                for (final AnnotationConfig config : configs) {
                    if (level.acceptScope(config.scope)) {
                        setTypeConfig(config.scope, new TypeConfig(config.processor, field, elementAnnotation));
                    }
                    else {
                        throw new IllegalProcessingScopeException(level, config.scope);
                    }
                }
            }
            else if (!addTypeConfig(elementAnnotation, field, elementAnnotation, level)) {
                forEachAnnotation(
                        elementAnnotation.annotationType(),
                        annotation -> addTypeConfig(annotation, field, elementAnnotation, level));
            }
        });
    }

    private boolean addTypeConfig(
            final Annotation annotation,
            final Field field,
            final Annotation elementAnnotation,
            final ProcessingScopeLevel level) {
        if (annotation instanceof ProcessedBy) {
            final ProcessedBy processedBy = (ProcessedBy) annotation;
            if (level.acceptScope(processedBy.scope())) {
                final AnnotationProcessor processor = Factory.inject(processedBy.value());
                final ProcessingScope scope = level.translate(processedBy.scope());
                setTypeConfig(scope, new TypeConfig(processor, field, elementAnnotation));
                return true;
            }
            throw new IllegalProcessingScopeException(level, processedBy.scope());
        }
        return false;
    }

    private void forEachAnnotation(final AnnotatedElement element, final Consumer<Annotation> consumer) {
        final Annotation[] declaredAnnotations = element.getDeclaredAnnotations();
        stream(declaredAnnotations).forEach(consumer);
    }

    private void setTypeConfig(final ProcessingScope scope, final TypeConfig config) {
        typeConfigs.addTo(scope, config);
    }

    Collection<TypeConfig> getTypeConfigs(final ProcessingScope scope) {
        return typeConfigs.getOrDefault(scope, Collections.emptySet());
    }
}
