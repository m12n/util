package org.m12n.util.factory;

public class IllegalProcessingScopeException extends RuntimeException {
    public IllegalProcessingScopeException(final ProcessingScopeLevel defaultScope, final ProcessingScope scope) {
        super("Illegal processing scope " + scope + " at " + defaultScope);
    }
}
