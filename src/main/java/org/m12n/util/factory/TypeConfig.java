package org.m12n.util.factory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

class TypeConfig {
    final AnnotationProcessor processor;
    final Field field;
    final Annotation annotation;

    TypeConfig(final AnnotationProcessor processor, final Field field, final Annotation annotation) {
        this.processor = processor;
        this.field = field;
        this.annotation = annotation;
    }
}
