package org.m12n.util.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;

/**
 * The Factory is a lightweight class that replaces a container and can be used
 * instead of dependency injection. Although it is lightweight, it is also very
 * powerful and allows instantiation of non-public classes, interfaces etc.
 * <br>
 * Usage:
 * <br>
 * Explicit instantiation:
 * <br>
 * {@code Example e = Factory.create(Example.class);}
 * <br>
 * Explicit reference:
 * <br>
 * {@code Example e = Factory.inject(Example.class);}
 * <br>
 * It also uses {@link AnnotationHandler} to process annotations.
 */
public class Factory {
    private static final Map<Class<?>, Object> instances = new ConcurrentHashMap<>();

    static {
        reset();
    }

    private Factory() {
        throw new IllegalAccessError("Don't instantiate");
    }

    /**
     * Returns the instance of the given type. The Factory guarantees, that only
     * one instance exists of each type.
     * <p>
     * If no instance exists, it uses {@link #newInstance(Class)} to create the instance.
     *
     * @param type The base type, can be a class or an interface.
     * @return The factory instance of the type.
     * @see #inject(Class)
     * @see #newInstance(Class)
     */
    public static <T> T getInstance(final Class<T> type) {
        synchronized (Factory.class) {
            T instance = (T) instances.get(type);
            if (instance == null) {
                instances.put(type, instance = newInstance(type));
            }
            return instance;
        }
    }

    /**
     * Synonym to {@link #getInstance(Class)}.
     */
    public static <T> T inject(final Class<T> type) {
        return getInstance(type);
    }

    /**
     * Creates a new instance of the type. This is a helper method that can be used regardless of
     * utilizing other Factory features.
     * <p>
     * This method instantiates types in the following ways:
     * <ul>
     * <li> Using reflection on a class
     * <li> Using reflection on a class' constructor (which works even for non-public classes)
     * <li> Using the {@link DefaultImpl} annotation on interfaces or abstract classes.
     * </ul>
     * It also processes the annotation of this type using {@link AnnotationHandler}.
     *
     * @param type The type that will be generated.
     * @return A new instance of the type.
     * @throws FactoryException if it wasn't possible to create an instance.
     * @see #getInstance(Class)
     * @see #create(Class)
     */
    public static <T> T newInstance(final Class<T> type) {
        T instance = null;
        if (!type.isInterface() && !Modifier.isAbstract(type.getModifiers())) {
            try {
                instance = type.newInstance();
            } catch (final InstantiationException | IllegalAccessException e) {
                instance = newInstanceConstructor(type);
            }
        }
        instance = AnnotationHandler.process(instance, type);
        if (!instances.containsKey(type)) {
            instances.put(type, instance);
        }
        return instance;
    }

    /**
     * Synonym of {@link #newInstance(Class)}
     */
    public static <T> T create(final Class<T> type) {
        return newInstance(type);
    }

    /**
     * Sets the instance of the class, that can be accessed through {@link #getInstance(Class)}.
     * If an instance already exists, it is overwritten by the given instance.
     *
     * @param type     The type.
     * @param instance The instance.
     * @see #define(Class, Object)
     * @see #getInstance(Class)
     */
    public static <T, I extends T> void setInstance(final Class<T> type, final I instance) {
        synchronized (Factory.class) {
            instances.put(type, instance);
        }
    }

    /**
     * Synonym of {@link #setInstance(Class, Object)}
     */
    public static <T, I extends T> void define(final Class<T> type, final I instance) {
        setInstance(type, instance);
    }

    /**
     * Completely resets the factory and clears all existing instances.
     */
    public static void reset() {
        instances.clear();
        AnnotationHandler.reset();
        AnnotationHandler.register(Inject.class, new InjectFieldProcessor(), ProcessingScope.PROCESS_FIELD);
    }

    private static <T> T newInstanceConstructor(final Class<T> type) {
        try {
            final Constructor<T>[] constructors = (Constructor<T>[]) type.getDeclaredConstructors();
            Constructor<T> constructor = null;
            int rank = 0;
            for (final Constructor<T> tmp : constructors) {
                final int tmpRank = rank(tmp);
                if (constructor == null ||
                        (rank < tmpRank &&
                                constructor.getParameterCount() > tmp.getParameterCount())) {
                    constructor = tmp;
                    rank = tmpRank;
                }
            }

            final Object[] params = new Object[constructor.getParameterCount()];
            final Class<?>[] paramTypes = constructor.getParameterTypes();
            for (int i = 0; i < params.length; i++) {
                params[i] = inject(paramTypes[i]);
            }

            constructor.setAccessible(true);
            return constructor.newInstance(params);
        } catch (SecurityException
                | InstantiationException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException e) {
            throw new FactoryException(type, e);
        }
    }

    private static int rank(final Constructor constructor) {
        final int modifiers = constructor.getModifiers();
        if (Modifier.isPublic(modifiers)) {
            return 4;
        }
        if (Modifier.isProtected(modifiers)) {
            return 3;
        }
        if (Modifier.isPrivate(modifiers)) {
            return 1;
        }
        return 2;
    }
}
