package org.m12n.util.factory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to specify the default implementation of an interface or an abstract class.
 * <p>
 * Example:
 * 
 * <pre>
 * <code>
 * 	&#64;{@link DefaultImpl}(MyClass.class)
 * 	public interface MyInterface {
 * 		...
 * 	}
 * ---
 * 	public class MyClass implements MyInterface {
 * 		...
 * 	}
 * ---
 * 	// inject interface like this:
 * 	MyInterface myInterface = Factory.inject(MyInterface.class);
 * </code>
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@ProcessedBy(value = DefaultImplTypeConstructor.class, scope = ProcessingScope.CONSTRUCT_TYPE)
public @interface DefaultImpl {
	Class<?> value();
}
