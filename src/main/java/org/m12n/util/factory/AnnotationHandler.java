package org.m12n.util.factory;

import org.m12n.util.Exceptions;
import org.m12n.util.Maps;
import org.m12n.util.MultiMap;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 * Processes all annotations on a type using the following lifecycle:
 * </p>
 * <ul>
 * <li>Construct Type - executed before the type is actually created. Can be used to instantiate interfaces or abstract
 * classes.</li>
 * <li>Preprocess Type - processes the type after construction, but before all fields are processed.</li>
 * <li>Process Field - processes each field of the type.</li>
 * <li>Postprocess Type - postprocessing of the type, after all fields have been processed.</li>
 * </ul>
 * <p>
 * This class is used by {@link Factory}.
 * </p>
 * <p>
 * Annotations are processed if they match the following criteria:
 * </p>
 * <ul>
 * <li>A {@link ProcessedBy} annotation</li>
 * <li>Any annotation that itself is annotated by {@link ProcessedBy}</li>
 * <li>Any annotation that has a registered {@link AnnotationProcessor} using the {@link #register(Class, AnnotationProcessor, ProcessingScope)}
 * method.</li>
 * </ul>
 * <p>Processing order of annotations is not guaranteed.</p>
 */
@SuppressWarnings("unchecked")
public class AnnotationHandler {
    private static final Map<Class<?>, TypeProfile> profiles = new ConcurrentHashMap<>();
    private static final MultiMap<Class<? extends Annotation>, AnnotationConfig> annotationConfigs = Maps.newMultiMap();

    private AnnotationHandler() {
        throw new IllegalAccessError("Don't instantiate");
    }

    /**
     * Registers {@link AnnotationProcessor}s for the given annotation type.
     *
     * @param annotationType The type of the annotation
     * @param processor      The {@link AnnotationProcessor}, that performs the processing
     * @param scope          The scope
     * @throws IllegalArgumentException if the scope is {@link ProcessingScope#DEFAULT}
     */
    public static void register(
            final Class<? extends Annotation> annotationType,
            final AnnotationProcessor processor,
            final ProcessingScope scope) {
        if (scope == ProcessingScope.DEFAULT) {
            throw new IllegalArgumentException("DEFAULT not allowed.");
        }
        annotationConfigs.addTo(annotationType, new AnnotationConfig(processor, scope));
    }

    /**
     * Resets the type profiles {@link #process(Object)} and the registered {@link AnnotationProcessor}s
     * {@link #register(Class, AnnotationProcessor, ProcessingScope)}.
     */
    public static void reset() {
        profiles.clear();
        annotationConfigs.clear();
    }

    /**
     * Processes the annotations of an object. Creates a "type profile" of each object that contains a description
     * of all annotations of the type. This profile is cached until {@link #reset()} is called.
     *
     * @param obj The object
     * @param <T> The type of the object
     * @return The processed object
     * @throws NullPointerException if the object is null
     */
    public static <T> T process(final T obj) {
        final Class<T> type = (Class<T>) obj.getClass();
        return process(obj, type);
    }

    /**
     * Processes the annotations of an object. Creates a "type profile" of each object that contains a description
     * of all annotations of the type. This profile is cached until {@link #reset()} is called.
     *
     * @param obj  The object that is processed. Can be null
     * @param type The type of the object
     * @param <T>  The type of the object
     * @return The object
     */
    public static <T> T process(T obj, final Class<T> type) {
        TypeProfile<T> profile = profiles.get(type);
        if (profile == null) {
            profiles.put(type, profile = new TypeProfile<>(type, annotationConfigs));
        }

        for (final ProcessingScope scope : ProcessingScope.processingOrder()) {
            for (final TypeConfig config : profile.getTypeConfigs(scope)) {
                try {
                    final T tmp = (T) config.processor.apply(type, obj, config.field, config.annotation);
                    if (tmp != null) {
                        obj = tmp;
                    }
                }
                catch (final Exception e) {
                    throw Exceptions.asRuntimeException(e, IllegalStateException::new);
                }
            }
        }

        return obj;
    }
}
