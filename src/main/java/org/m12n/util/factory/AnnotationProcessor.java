package org.m12n.util.factory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Process annotation on classes or fields.
 *
 * @param <A> The type of the annotation that is processed.
 */
public interface AnnotationProcessor<T, A extends Annotation> {
    /**
     * Applies the processor on an instance or field.
     *
     *
     * @param type       The type of the {@code obj}.
     * @param obj        The instance that is processed. Can be {@code null}.
     * @param field      The field of the instance that is processed. Can be {@code null}, if the annotation is
     *                   processed on a class, not a field.
     * @param annotation The annotation that is processed.
     * @throws Exception Can throw any {@link Exception}.
     */
    T apply(final Class<T> type, final T obj, final Field field, final A annotation) throws Exception;
}
