package org.m12n.util.factory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Abstract class to process a field.
 *
 * @param <T> Type of the object
 * @param <A> Type of the annotation
 */
public abstract class FieldProcessor<T, A extends Annotation> implements AnnotationProcessor<T, A> {
    protected abstract void processField(T obj, Field field, A annotation) throws Exception;

    @Override
    public final T apply(final Class<T> type, final T obj, final Field field, final A annotation) throws Exception {
        processField(obj, field, annotation);
        return obj;
    }
}
