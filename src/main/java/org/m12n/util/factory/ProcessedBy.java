package org.m12n.util.factory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Provides the {@link AnnotationProcessor} and {@link ProcessingScope} of an annotation.
 * </p>
 * <p>
 * Can be also directly attached to a type or a field.
 * </p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface ProcessedBy {
    Class<? extends AnnotationProcessor> value();

    ProcessingScope scope() default ProcessingScope.DEFAULT;
}
