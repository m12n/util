package org.m12n.util.factory;

import static org.m12n.util.factory.ProcessingScope.PREPROCESS_TYPE;
import static org.m12n.util.factory.ProcessingScope.PROCESS_FIELD;

enum ProcessingScopeLevel {
    TYPE(PREPROCESS_TYPE) {
        @Override
        boolean acceptScope(final ProcessingScope scope) {
            return scope.isType();
        }
    },
    FIELD(PROCESS_FIELD) {
        @Override
        boolean acceptScope(final ProcessingScope scope) {
            return scope.isField();
        }
    };

    private final ProcessingScope value;

    ProcessingScopeLevel(final ProcessingScope value) {
        this.value = value;
    }

    abstract boolean acceptScope(ProcessingScope scope);

    ProcessingScope translate(final ProcessingScope scope) {
        return scope == ProcessingScope.DEFAULT ? value : scope;
    }
}
