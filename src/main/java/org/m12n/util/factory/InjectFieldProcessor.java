package org.m12n.util.factory;

import javax.inject.Inject;
import java.lang.reflect.Field;

/**
 * Injects fields annotated by the {@link Inject} annotation.
 */
class InjectFieldProcessor extends FieldProcessor<Object, Inject> {
    @Override
    protected void processField(final Object obj, final Field field, final Inject annotation) throws Exception {
        field.set(obj, Factory.inject(field.getType()));
    }
}
