package org.m12n.util.factory;

class AnnotationConfig {
    final AnnotationProcessor processor;
    final ProcessingScope scope;

    public AnnotationConfig(final AnnotationProcessor processor, final ProcessingScope scope) {
        this.processor = processor;
        this.scope = scope;
    }
}
