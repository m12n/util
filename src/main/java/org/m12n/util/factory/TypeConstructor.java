package org.m12n.util.factory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Abstract class to construct a type.
 *
 * @param <T> Type of the object
 * @param <A> Type of the annotation
 */
public abstract class TypeConstructor<T, A extends Annotation> implements AnnotationProcessor<T, A> {
    protected abstract T constructType(Class<T> type, A annotation) throws Exception;

    @Override
    public final T apply(final Class<T> type, final T obj, final Field field, final A annotation) throws Exception {
        return constructType(type, annotation);
    }
}
