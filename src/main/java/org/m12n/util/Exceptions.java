package org.m12n.util;

import org.m12n.util.function.ThrowingRunnable;
import org.m12n.util.function.ThrowingSupplier;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Utility methods to work with exceptions.
 */
public class Exceptions {
    private Exceptions() {
        throw new IllegalAccessError("Don't instantiate");
    }

    /**
     * Converts an exception to a {@link RuntimeException} if it isn't already.
     *
     * @param original The original exception
     * @param wrapper  The wrapper, wrapping the original exception if it is not a {@link RuntimeException}
     * @return A {@link RuntimeException}, either wrapping the original or being the original
     */
    public static RuntimeException asRuntimeException(
            final Throwable original,
            final Function<Throwable, RuntimeException> wrapper) {
        if (original instanceof RuntimeException) {
            return (RuntimeException) original;
        }
        return wrapper.apply(original);
    }

    /**
     * Converts an exception to a {@link RuntimeException} if it isn't already.
     *
     * @param original The original exception
     * @return A {@link RuntimeException}, either wrapping the original or being the original
     */
    public static RuntimeException asRuntimeException(final Throwable original) {
        return asRuntimeException(original, RuntimeException::new);
    }

    /**
     * Silently converts any thrown exception into {@link IllegalStateException}s.
     */
    public static void sneakyThrows(final ThrowingRunnable runnable) {
        try {
            runnable.run();
        }
        catch (final Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Silently converts any thrown exception into {@link IllegalStateException}s.
     */
    public static <T, E extends Exception> T sneakyThrows(final ThrowingSupplier<T, E> producer) {
        try {
            return producer.get();
        }
        catch (final Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Swallows all thrown exceptions.
     */
    public static void safeRun(final ThrowingRunnable runnable) {
        safeRun(runnable, null);
    }

    /**
     * If an exception is thrown, it will be used to call the {@code exceptionConsumer}, which can be used for logging
     * or other handling.
     */
    public static <E extends Exception> void safeRun(final ThrowingRunnable<E> runnable, final Consumer<E> consumer) {
        try {
            runnable.run();
        }
        catch (final Exception e) {
            handleException((E) e, consumer);
        }
    }

    /**
     * Swallows all thrown exceptions.
     *
     * @return the result of the {@link ThrowingSupplier} as {@link Optional}, which is empty if an exception has been
     * thrown.
     */
    public static <T, E extends Exception> Optional<T> safeRun(final ThrowingSupplier<T, E> producer) {
        return safeRun(producer, null);
    }

    /**
     * If an exception is thrown, it will be used to call the {@code exceptionConsumer}, which can be used for logging
     * or other handling (but still returning an empty {@link Optional}).
     *
     * @return the result of the {@link ThrowingSupplier} as {@link Optional}, which is empty if an exception has been
     * thrown.
     */
    public static <T, E extends Exception> Optional<T> safeRun(final ThrowingSupplier<T, E> producer, final Consumer<E> consumer) {
        try {
            return Optional.ofNullable(producer.get());
        }
        catch (final Exception e) {
            handleException((E) e, consumer);
            return Optional.empty();
        }
    }

    private static <E extends Exception> void handleException(final E e, final Consumer<E> consumer) {
        if (consumer != null) {
            consumer.accept(e);
        }
    }
}
