package org.m12n.util.function;

@FunctionalInterface
public interface ThrowingFunction<V, R, E extends Exception> {
    R apply(V value) throws E;
}
