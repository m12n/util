# Collection of utility classes and methods

[![Build Status](https://travis-ci.org/m12n/util.svg?branch=master)](https://travis-ci.org/m12n/util) [![Coverage Status](https://coveralls.io/repos/github/m12n/util/badge.svg?branch=master)](https://coveralls.io/github/m12n/util?branch=master)

## Usage

Add the following dependency:

```
<dependency>
    <groupId>${project.groupId}</groupId>
    <artifactId>${project.artifactId}</artifactId>
    <version>${project.version}</version>
</dependency>
```

Add the following repository:

```
<repository>
    <id>m12n-maven-repo-releases</id>
    <url>http://maven.m12n.org/releases</url>
</repository>
```

## Factory

`Factory` started as a simple alternative to a "heavyweight" container,
such as Spring. The main benefit is, that Factory doesn't need 
configuration or classpath scanning.

Any object created by `Factory` will be processed by the 
`AnnotationHandler`.


### Type instantiation

With Factory, you can create any class using reflection:

```
MyObject obj = Factory.newInstance(MyObject.class);
```

> Note that `Factory.create()` is an alias to `newInstance()`.

There is no further configuration required, but you can of course use
annotations to perform additional tasks (such as dependency injection).


### Singleton instantiation

In order to produce only one singleton instance, you can also use the 
`Factory`:

```
MyObject obj = Factory.getInstance(MyObject.class);
```

> Note that `Factory.inject()` is an alias.

With `getInstance()` Factory guarantees that only one instance will be
produced.


### Annotation processing

`Factory` uses the `AnnotationHandler` (which can also be used 
stand-alone without `Factory`), to process annotation of the type.

Annotations can be defined for types or for fields (including private
fields).

The following lifecycle phases (scopes) will be managed:

1. `CONSTRUCT_TYPE` - before instantiating an object. This scope can be
used to create default instances of interfaces or abstract classes.
2. `PREPROCESS_TYPE` - after instantiating, but before processing the
annotations of fields.
3. `PROCESS_FIELD` - annotation processing of the fields.
4. `POSTPROCESS_TYPE` - after processing the fields.

`AnnotationProcessor`s can be defined in three ways:


#### Registration

Registering an annotation using `AnnotationHandler.register()`. This 
can be used to register processors to standard annotations, such as 
`javax.inject.Inject`:

```
AnnotationHandler.register(Inject.class, new InjectFieldProcessor(), ProcessingScope.PROCESS_FIELD);
```

> Note, that the `javax.inject.Inject` annotation will be processed
by the `Factory` by default.


#### Direct annotation

Using the `@ProcessedBy` annotation at type or field level directly.
This has the advantage, that it is not necessary to define additional 
custom annotations, but has the disadvantage, that it is not possible 
to pass additional parameters to the processor:

```
@ProcessedBy(MyTypeProcessor.class)
public class MyObject {
    @ProcessedBy(MyFieldProcessor.class)
    private String myField;
}
```


#### Custom annotation

Using a custom annotation, annotated by `@ProcessedBy`:

```
// Define the annotation
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@ProcessedBy(value = GreetingProcessor.class)
@interface SetName {
    String value() default "";
}

// Define the processor
public class GreetingProcessor extends FieldProcessor<Object, SetName> {
    @Override
    protected void processField(final Object obj, final Field field, final SetName annotation) throws Exception {
        field.set(obj, "Hello, " + annotation.value() + "!");
    }
}

// Use the annotation
public class MyObject {
    @SetName("World")
    private String greeting;

    @Override
    public String toString() {
        return greeting;
    }
}

// Use the factory
MyObject obj = Factory.create(MyObject.class);
System.out.println(obj); // prints: "Hello, World!"
```

> Note, that instead of `Factory.create()`, you could also use 
`AnnotationHandler.process(obj)`.


## Function

Extension to the `java.util.function` package, providing functional
interfaces that allow exceptions to be thrown.

## Exceptions

Utility methods to ease exception handling:

- Converting checked exceptions into runtime exceptions

- Swallowing exceptions

- Converting exceptions

## Maps

Utility methods to easily create maps.

## MultiMap

`MultiMap` is a simple extendsion to a `Map` defining a collection of
values as the map value. Can be easily created using `Maps.newMultiMap()`,
but other implementations are also possible.
